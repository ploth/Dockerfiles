This image is capable of compiling some android source branches.
Tested with `aosp_h3213-eng` and `aosp_h3213-userdebug`

| Branch            | Status      |
| :-                | :-          |
| android-8.1.0_r35 | Working     |
| android-8.1.0_r51 | Working     |
| android-9.0.0_r1  | Working     |
| android-9.0.0_r16 | Working     |
| android-9.0.0_r30 | Working     |

### Prerequisites

Initialised AOSP tree (`repo sync` and `./repo_update.sh`)

### Build android

```bash
docker build -t android - < /path/to/Dockerfile
docker run -it --mount type=bind,source=/path/to/android,target=/mnt android bash
# (inside container)
cd /mnt
source build/envsetup.sh && lunch
make –j <insert the cpu thread number of your computer>
```

